package be.kdg.chefs.model

data class Kookboek(val id: Int, val titel: String, var uitgave: Int, val isbn: String, var aantal_paginas: Int, val chef_id: Int)