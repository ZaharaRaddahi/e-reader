package be.kdg.chefs.model

import be.kdg.chefs.R
import java.util.*

fun getChefs(): Array<Chef>{
    val chef1 = Chef(1, naam = "Alain Ducasse", geboortedatum = GregorianCalendar(1956,9,13), nationaliteit = "Monaco", restaurant = "Alain Ducasse au Plaza Athénée",
        keuken = "Nouvelle cuisine", michelin_sterren = 3, image =  R.drawable.alain_ducasse)

    val chef2 = Chef(2, naam = "Gordon James Ramsay", geboortedatum = GregorianCalendar(1966,11,8), nationaliteit = "Verenigd Koninkrijk", restaurant = "Restaurant Gordon Ramsay",
        keuken = "Engelse keuken", michelin_sterren = 3, image =  R.drawable.gordon_ramsay)

    val chef3 = Chef(3, naam = "Anne-Sophie Pic", geboortedatum = GregorianCalendar(1969,7,12), nationaliteit = "Frankrijk", restaurant = "Le 7",
        keuken = "Nouvelle cuisine", michelin_sterren = 3, image =  R.drawable.anne_sophie_pic)

    val chef4 = Chef(4, naam = "Jamie Oliver", geboortedatum = GregorianCalendar(1975,5,27), nationaliteit = "Verenigd Koninkrijk", restaurant = "Fifteen",
        keuken = "Fusion cuisine", michelin_sterren = 0, image =  R.drawable.jamie_oliver)

    val chef5 = Chef(5, naam = "Piet Huysentruyt", geboortedatum = GregorianCalendar(1962,12,7), nationaliteit = "België", restaurant = "Likoké",
        keuken = "Belgische keuken", michelin_sterren = 1, image =  R.drawable.piet_huysentruyt)

    val chef6 = Chef(6, naam = "Sergio Herman", geboortedatum = GregorianCalendar(1970,5,5), nationaliteit = "Nederland", restaurant = "The Jane",
        keuken = "Franse keuken", michelin_sterren = 1, image =  R.drawable.sergio_herman)

    val chef7 = Chef(7, naam = "Peter Goossens", geboortedatum = GregorianCalendar(1964,4,6), nationaliteit = "België", restaurant = "Hof van Cleve",
        keuken = "Hof van Cleve", michelin_sterren = 3, image =  R.drawable.peter_goossens)

    val chef8 = Chef(8, naam = "Angélique Schmeinck", geboortedatum = GregorianCalendar(1964,6,4), nationaliteit = "Nederland", restaurant = "Restaurant De Kromme Dissel",
        keuken = "Franse keuken", michelin_sterren = 1, image =  R.drawable.angelique_schmeinck)

    val chef9 = Chef(9, naam = "Gastón Acurio", geboortedatum = GregorianCalendar(1967,10,30), nationaliteit = "Peru", restaurant = "Astrid & Gastón",
        keuken = "Peruviaanse keuken", michelin_sterren = 0, image =  R.drawable.gaston_acurio)

    val chef10 = Chef(10, naam = "Stéphane Buyens", geboortedatum = GregorianCalendar(1960,3,20), nationaliteit = "België", restaurant = "Hostellerie Le Fox",
        keuken = "Franse keuken", michelin_sterren = 2, image =  R.drawable.stephane_buyens)

    val chef11 = Chef(11, naam = "Roger van Damme", geboortedatum = GregorianCalendar(1971,4,18), nationaliteit = "België", restaurant = "Het Gebaar",
        keuken = "Franse keuken", michelin_sterren = 1, image =  R.drawable.roger_van_damme)

    val chef12 = Chef(12, naam = "Johan Segers", geboortedatum = GregorianCalendar(1949,8,18), nationaliteit = "België", restaurant = "'t Fornuis",
        keuken = "Belgische keuken", michelin_sterren = 1, image =  R.drawable.johan_segers)

    val chef13 = Chef(13, naam = "Alfredo Russo", geboortedatum = GregorianCalendar(1968,4,10), nationaliteit = "Italië", restaurant = "Dolce Stil Novo",
        keuken = "Italiaanse keuken", michelin_sterren = 1, image =  R.drawable.alfredo_russo)

    val chef14 = Chef(14, naam = "René Redzepi", geboortedatum = GregorianCalendar(1977,12,16), nationaliteit = "Denemarken", restaurant = "Noma",
        keuken = "Noordse keuken", michelin_sterren = 2, image =  R.drawable.rene_redzepi)

    val chef15 = Chef(15, naam = "Hans van Wolde", geboortedatum = GregorianCalendar(1967,11,23), nationaliteit = "Nederland", restaurant = "Beluga",
        keuken = "Franse keuken", michelin_sterren = 2, image =  R.drawable.hans_van_wolde)
    return arrayOf(chef1, chef2, chef3, chef4, chef5, chef6, chef7, chef8, chef9, chef10, chef11, chef12, chef13, chef14, chef15)
}