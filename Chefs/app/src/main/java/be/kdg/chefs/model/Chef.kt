package be.kdg.chefs.model

import java.util.*

data class Chef(val id: Int, val naam : String, val geboortedatum : GregorianCalendar, var nationaliteit : String, var restaurant : String,
           var keuken : String, var michelin_sterren: Int, var image : Int)